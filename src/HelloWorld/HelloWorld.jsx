import React from "react";
import { AppContext } from "../store";

const HelloWorld = function() {
  console.log("Render");

  return (
    <AppContext.Provider>
      <p>Hello, World!</p>
    </AppContext.Provider>
  );
};

export default HelloWorld;
