import React from "react";
import App from "../App/App";
import HelloWorld from "../HelloWorld/HelloWorld";

const Root = function() {
  console.log("Root");

  return (
    <div>
      <App />
      <HelloWorld />
    </div>
  );
};

export default Root;
