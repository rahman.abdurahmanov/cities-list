import React, { useEffect } from "react";

const CityItem = function({ city, key, onDelete }) {
  const willUnmount = () => {
    return function() {
      console.log("City was deleted!");
    };
  };

  useEffect(willUnmount, []);

  return (
    <li key={key}>
      <p>{city}</p>
      <button onClick={onDelete}>delete</button>
    </li>
  );
};

export default CityItem;
