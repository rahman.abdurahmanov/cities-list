import React, { useState, useContext } from "react";
import { AppContext, ActionTypes } from "../store";

const CitiesInput = function({ placeholder = "City name" }) {
  const [city, setCity] = useState("");
  const { citiesDispatcher } = useContext(AppContext);

  const addCity = event => {
    if (event.key === "Enter") {
      citiesDispatcher({ type: ActionTypes.ADD, payload: city });
      setCity("");
    }
  };

  const onChange = event => {
    setCity(event.target.value);
  };

  return (
    <input
      type="text"
      placeholder={placeholder}
      value={city}
      onChange={onChange}
      onKeyPress={addCity}
    />
  );
};

export default CitiesInput;
