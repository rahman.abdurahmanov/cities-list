import React, { useEffect, useReducer } from "react";
import { AppContext, citiesReducer } from "../store";
import CitiesInput from "../CityInput/CityInput";
import CitiesList from "../CitiesList/CitiesList";
import "./App.css";

const App = function() {
  const initCities = () => {
    return JSON.parse(localStorage.getItem("cities")) || [];
  };

  const [cities, citiesDispatcher] = useReducer(citiesReducer, initCities());

  const didUpdate = () => {
    localStorage.setItem("cities", JSON.stringify(cities));
  };

  useEffect(didUpdate, [cities]);

  return (
    <AppContext.Provider value={{ cities, citiesDispatcher }}>
      <CitiesInput />
      <CitiesList />
    </AppContext.Provider>
  );
};

export default App;
