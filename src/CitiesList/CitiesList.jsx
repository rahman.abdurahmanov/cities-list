import React, { useContext } from "react";
import CityItem from "../CityItem/CityItem";
import { AppContext, ActionTypes } from "../store";

const CitiesList = function() {
  const { cities, citiesDispatcher } = useContext(AppContext);

  return (
    <ul>
      {cities.map((city, key) => (
        <CityItem
          city={city}
          key={key}
          onDelete={() => {
            citiesDispatcher({
              type: ActionTypes.DELETE,
              payload: city
            });
          }}
        />
      ))}
    </ul>
  );
};

export default CitiesList;
