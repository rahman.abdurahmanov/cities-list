import { createContext } from "react";

export const AppContext = createContext();

export const ActionTypes = {
  ADD: "ADD",
  DELETE: "DELETE"
};

export const citiesReducer = function(cities, action) {
  switch (action.type) {
    case ActionTypes.ADD: {
      return [...cities, action.payload];
    }
    case ActionTypes.DELETE: {
      return cities.filter(item => item !== action.payload);
    }
    default:
      return cities;
  }
};
